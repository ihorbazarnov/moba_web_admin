<?php
/**
 * Moba functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Moba
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function moba_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on Moba, use a find and replace
		* to change 'moba' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'moba', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Primary', 'moba' ),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'moba_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'moba_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function moba_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'moba_content_width', 640 );
}
add_action( 'after_setup_theme', 'moba_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function moba_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'moba' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'moba' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'moba_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function moba_scripts() {
}
add_action( 'wp_enqueue_scripts', 'moba_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


add_action('init', 'register_post_type_portfolio', 1);
function register_post_type_portfolio() {
register_post_type('portfolio', array(
	'label'              => 'Portfolio',
	'labels'             => array(
		'name'               => 'Portfolio',
		'singular_name'      => 'Portfolio',
		'menu_name'          => 'Cases',
		'add_new'            => 'Add case',
		'add_new_item'       => 'Add Case',
		'new_item'           => 'New Case',
		'edit_item'          => 'Edit Case',
		'view_item'          => 'View case',
		'all_items'          => 'All cases',
		'search_items'       => 'Search case',
		'not_found_in_trash' => 'Trash is empty'
	),
	'public'             => true,
	'publicly_queryable' => true,
	'show_ui'            => true,
	'show_in_menu'       => true,
	'show_in_nav_menus'  => false,
	// 'show_in_rest'       => true,
	'query_var'          => true,
	'taxonomies'         => array('types'),
	'menu_icon'          => 'dashicons-cart',
	'hierarchical'       => false,
	'menu_position'      => 22,
	'supports'           => array('title', 'editor', 'thumbnail', 'excerpt')
));
register_taxonomy( 'types', array( 'portfolio' ), array(
	'hierarchical'      => true,
	'public'            => true,
	'show_in_nav_menus' => true,
	'show_ui'           => true,
	'show_admin_column' => true,
	'query_var'         => true,
	// 'rewrite'           => array('slug' => 'products'),
	'capabilities'      => array(
		'manage_terms'  => 'edit_posts',
		'edit_terms'    => 'edit_posts',
		'delete_terms'  => 'edit_posts',
		'assign_terms'  => 'edit_posts'
	),
	'labels'            => array(
		'name'                       => 'Category',
		'singular_name'              => 'Category',
		'taxonomy general name'      => 'Category',
		'search_items'               => 'Search catalog categories',
		'popular_items'              => 'Popular categories',
		'all_items'                  => 'All categories',
		'parent_item'                => 'Parent category',
		'parent_item_colon'          => 'Parent category:',
		'edit_item'                  => 'Edit category',
		'update_item'                => 'Update category',
		'add_new_item'               => 'New category',
		'new_item_name'              => 'Add category',
		'add_or_remove_items'        => 'Add or delete category',
		'choose_from_most_used'      => 'Choose from most popular categories',
		'menu_name'                  => 'Categories',
	),
) );

flush_rewrite_rules( false );

};

function home_portfolio( $get ) {
	$cases = get_posts(['post_type' => 'portfolio']);
	$arr = [];
	foreach ($cases as $case) {
		$id = $case->ID;
		$image = get_field('image_preview', $id);
		$title = get_field('title', $id);
		$slug = basename(get_permalink($id));
		$arr[] = ['image' => $image, 'title' => $title, 'slug' => $slug];
	}
    return $arr;
}
function portfolio_page($get) {
	$cat_args = array(
		'orderby'     => 'term_id',
		'order'       => 'ASC',
		'hide_empty'  => true
	);
	$cats = get_terms('types', $cat_args);
	$arr = [];
	foreach ($cats as $cat) {
		$posts = [];
		$post_args = array(
			'posts_per_page' => -1,
			'post_type'      => 'portfolio',
			'tax_query'      => array(
				'taxonomy'   => 'type',
				'field'      => 'term_id',
				'terms'      => $cat->term_id
			)
		);
		$cases = get_posts(['post_type' => 'portfolio']);
		foreach ($cases as $case) {
			$id = $case->ID;
			$image = get_field('image_preview', $id);
			$title = get_field('title', $id);
			$text = get_field('short_description', $id);
			$slug = basename(get_permalink($id));
			$posts[] = ['image' => $image, 'title' => $title, 'text' => $text, 'slug' => $slug];
		}
		$total = [
			'name' => $cat->name,
			'list' => $posts
		];
		$arr[] = $total;
	}
	return $arr;
}
function single_case(WP_REST_Request $request) {
	// $slug = $get;
	if (!empty($request && !empty($request['slug']))) {
		$slug = $request['slug'];
		$args = array(
			'name' => $slug,
			'post_type' => 'portfolio',
			'post_status' => 'publish',
			'numberposts' => 1
		);
		$post_query = get_posts($args);
		if ($post_query) {
			$post = array();
			$id = $post_query[0]->ID;
			$post['id'] = $id;
			$post['title'] = $post_query[0]->post_title;
			$post['c_title'] = get_field('title', $id);
			$post['short_text'] = get_field('short_description', $id);
			$post['client'] = get_field('client', $id);
			$post['task'] = get_field('task', $id);
			$post['year'] = get_field('year', $id);
			$post['img'] = get_field('image_preview', $id);
			$post['content_title'] = get_field('content_title', $id);
			$post['content_text'] = get_field('content_text', $id);
			$post['tags'] = get_field('content_tags', $id);
			$post['images'] = get_field('images_list', $id);
			$post['mobile'] = get_field('m_show', $id);
			$post['m_left'] = get_field('m_left_img', $id);
			$post['m_right'] = get_field('m_right_img', $id);
			return $post;
		}
	} else {
		return '0';
	}
}

function send_form($request_data) {
	$token = '5508230598:AAE7TQQMuu9F5ahalXbyp8eEU55p3np44NQ';
	$chat = '-1001720961020';
	$url = "https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chat}&parse_mode=html";
	$text = '&text=';
	if ($request_data['hidden'] === true) {
		return 1;
	} else {
		if ($request_data['email']) {
			$text .= "Email: ". $request_data['email'];
		}
		if ($request_data['phone']) {
			$text .= ' %0A Phone: '. $request_data['phone'];
		}
		if ($request_data['message']) {
			$text .= ' %0A Message: '. $request_data['message'];
		}
	}
	$sendToTelegram = fopen($url.$text,"r");
	// return wp_send_json_success( $_POST );
	// return $url.$text;
	if ($sendToTelegram) {
		return 1;
	} else {
		return 0;
	}
	wp_die();
	// return ['answer' => $sendToTelegram];
	// wp_send_json($request_data['test']);
}


function rest_callback() {
    register_rest_route( 'api/v1', '/home_portfolio', array(
        'methods' => 'GET',
        'callback' => 'home_portfolio'
    ));
    register_rest_route( 'api/v1', '/portfolio_page', array(
    	'methods' => 'GET',
    	'callback' => 'portfolio_page'
    ));
    register_rest_route( 'api/v1', '/case/(?P<slug>.+)', array(
    	'methods' => 'GET',
    	'callback' => 'single_case'
    ));
    register_rest_route('api/v1', '/form_feedback', array(
    	'methods' => 'POST',
    	'callback' => 'send_form',
    	'permission_callback' => '__return_true'
    ));
}
add_action( 'rest_api_init', 'rest_callback');

